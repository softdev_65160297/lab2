/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab02;

import java.util.Scanner;

/**
 *
 * @author paewnosuke
 */
public class Lab02 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int Row, Col;
    static char winner = '-';
    static boolean isFinish = false;
    static int round;
    static String inputContinue;

    static void printWelcome() {
        System.out.println("Welcome to TIC TAC TOE Game");
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }

    }

    static void printTurn() {
        System.out.println(currentPlayer + " turn");
    }

    static void printRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input Row Col : ");
            Row = kb.nextInt();
            Col = kb.nextInt();
            if (table[Row][Col] == '-') {
                table[Row][Col] = currentPlayer;
                round++;
                break;
            }

        }
    }

    static void swicthPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    static void checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[Row][i] != currentPlayer) {
                return;
            }
        }
        winner = currentPlayer;
        isFinish = true;

    }

    static void checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][Col] != currentPlayer) {
                return;
            }
        }
        isFinish = true;
        winner = currentPlayer;

    }

    static void isWin() {
        checkRow();
        checkCol();
        checkLeftCross();
        checkRightCross();
    }

    static void checkLeftCross() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return;
            }
        }
        isFinish = true;
        winner = currentPlayer;

    }

    static void checkRightCross() {
        int count = 2;
        for (int i = 0; i < 3; i++) {
            if (table[i][count] != currentPlayer) {
                return;
            }
            count--;
        }
        isFinish = true;
        winner = currentPlayer;

    }

    static boolean playAgain() {
        if (isFinish == true) {

            return true;
        }
        return false;
    }

    static boolean isNewgame() {
        if (playAgain()) {
            Scanner kb = new Scanner(System.in);
            System.out.println("Continue (y/n)");
            inputContinue = kb.next();
            if ("n".equals(inputContinue)) {
                
                System.out.println("Bye Bye~");
                return true;
            } 
        }return false;
            
    
    }

    static void printNewTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }
            System.out.println("");
        }

    }

    static void checkFinish() {
        if (round == 9) {
            isFinish = true;
        }
    }

    static void show() {
        printTable();
        if (winner != '-') {
            System.out.print(winner + " is Winner!!");
        } else {
            System.out.println("the game is draw");
        }
    }

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            do {
                printTable();
                printTurn();
                printRowCol();
                isWin();
                checkFinish();
                swicthPlayer();
            } while (!isFinish);
            {
                show();
                if (isNewgame()) {
                    break;
                }else{
                isFinish = false;
                round = 0;
                printNewTable();
            }

        }

    }

}
}
